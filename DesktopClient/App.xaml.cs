using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace DesktopClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ServiceReference1.Service1Client server;

        public App()
        {
            server = new ServiceReference1.Service1Client("WSHttpBinding_IService1", "http://localhost:4705/Service1.svc");
        }

        public static ServiceReference1.Service1Client Server()
        {
            return ((App)Application.Current).server;
        }
    }
}
