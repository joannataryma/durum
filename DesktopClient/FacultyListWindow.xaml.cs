using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DesktopClient
{
    /// <summary>
    /// Interaction logic for FacultyListWindow.xaml
    /// </summary>
    public partial class FacultyListWindow : Window
    {
        public FacultyListWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            System.Windows.Data.CollectionViewSource facultyCompositeTypeViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("facultyCompositeTypeViewSource")));
            facultyCompositeTypeViewSource.Source = App.Server().GetFaculties();
            facultyCompositeTypeViewSource.DeferRefresh();
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            this.Content = null;
            Window fac = new MainWindow();
            this.Close();
            fac.Show();
        }

        void ShowDetails(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow)
                {
                    var row = (DataGridRow)vis;
                    DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(row);

                    if (presenter == null)
                    {
                        facultyCompositeTypeDataGrid.ScrollIntoView(row, facultyCompositeTypeDataGrid.Columns[0]);
                        presenter = GetVisualChild<DataGridCellsPresenter>(row);
                    }

                    DataGridCell FacultyNameCell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(0);
                    var facultyName = FacultyNameCell.Content as TextBlock;
                    Window fac = new FacultyWindow(facultyName.Text);
                    fac.Show();
                    break;
                }
        }

        public static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }
    }
}
