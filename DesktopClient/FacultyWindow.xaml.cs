﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DesktopClient
{
    /// <summary>
    /// Interaction logic for FacultyWindow.xaml
    /// </summary>
    public partial class FacultyWindow : Window
    {
        string facultyName;
        ServiceReference1.FacultyCompositeType f;

        public FacultyWindow(string facultyName)
        {
            this.facultyName = facultyName;
            InitializeComponent();
            this.init();
        }

        private void init()
        {
            f = App.Server().GetFacultyByName(this.facultyName);
            ServiceReference1.TeacherCompositeType[] t = App.Server().GetTeachersByFacultyName(this.facultyName);
            this.Nazwa.Content = f.name;
            string prowadzacy = "";
            int i = 0;
            foreach (var x in t)
            {
                i++;
                prowadzacy += x.name;
                if(i!=t.Length)
                    prowadzacy += ", ";
            }
            this.Prowadzacy.Content = prowadzacy;
            this.Opis.Content = f.description;
            this.Kiedy.Content = f.time;
            this.Lokacja.Content = f.location.name;
        }

        private void zapiszButton_Click(object sender, RoutedEventArgs e)
        {
            bool result = false;
            var l = System.Windows.Application.GetCookie(new Uri("http://localhost:4705/Service1.svc"));
            try
            {
                result = App.Server().SignUp(f.id, l);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (result)
                MessageBox.Show("Zapisano na wybrany kurs!");
        }

        private void Lokacja_Click(object sender, RoutedEventArgs e)
        {
            Window map = new MapWindow(new MapWindow.LocationInfo("F", f.name, f.location.latitude, f.location.longitude));
            map.Show();
        }
    }
}
