using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DesktopClient
{
    public class NoteLine
    {
        private ServiceReference1.NoteCompositeType note;
        private ServiceReference1.FacultyCompositeType faculty;

        public NoteLine(ServiceReference1.NoteCompositeType n, ServiceReference1.FacultyCompositeType f)
        {
            note = n;
            faculty = f;
        }

        public string Message { get { return note.message; } }
        public string Time { get { return note.time; } }
        public string Teacher { get { return note.teacherName; } }
        public string Faculty { get { return faculty.name; } }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<NoteLine> notes = new ObservableCollection<NoteLine>();

        public ObservableCollection<NoteLine> Notes
        {
            get
            {
                return notes;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            bool x = this.MapAnimButton.IsEnabled;
            string y = this.MapAnimButton.Visibility.ToString();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var l = System.Windows.Application.GetCookie(new Uri("http://localhost:4705/Service1.svc"));
            string p = App.Server().CheckUserPrivileges(l);
            if (p.Equals("student"))
                NotkaButton.Visibility = Visibility.Hidden;
            var locationCompositeTypeViewSource = (System.Windows.Data.CollectionViewSource)(this.FindResource("locationCompositeTypeViewSource"));
            locationCompositeTypeViewSource.Source = App.Server().GetLocations();
            locationCompositeTypeViewSource.DeferRefresh();
            refreshNotes();
        }

        public void refreshNotes()
        {
            notes.Clear();
            foreach (var n in App.Server().GetAllNotes().Reverse())
            {
                ServiceReference1.FacultyCompositeType f = App.Server().GetFacultyById(n.facultyId);
                notes.Add(new NoteLine(n, f));
            }
        }

        private void fakultetyButton_Click(object sender, RoutedEventArgs e)
        {
            this.Content = null;
            Window fac = new FacultyListWindow();
            this.Close();
            fac.Show();
        }

        private void saveButton_Copy_Click(object sender, RoutedEventArgs e)
        {
            this.Content = null;
            Window tea = new TeacherListWindow();
            this.Close();
            tea.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Window notk = new NotkaWindow(this);
            notk.Show();
        }

        private void MapButton_Click(object sender, RoutedEventArgs e)
        {
            List<MapWindow.LocationInfo> locations = new List<MapWindow.LocationInfo>();
            foreach (var t in App.Server().GetTeachers())
                locations.Add(new MapWindow.LocationInfo("T", t.name, t.location.latitude, t.location.longitude));
            foreach (var f in App.Server().GetFaculties())
                locations.Add(new MapWindow.LocationInfo("F", f.name, f.location.latitude, f.location.longitude));

            Window map = new MapWindow(locations.ToArray());
            map.Show();
        }
    }
}
