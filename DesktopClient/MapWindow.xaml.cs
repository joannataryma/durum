﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Maps.MapControl.WPF;

namespace DesktopClient
{
    /// <summary>
    /// Interaction logic for MapWindow.xaml
    /// </summary>
    public partial class MapWindow : Window
    {
        private LocationInfo[] locations;
        private Location center;

        public class LocationInfo
        {
            public string   type;
            public string   desc;
            public Location location;

            private static Location fromString(string latstr, string lonstr)
            {
                double lat, lon;
                latstr = latstr.Replace('.', ',');
                lonstr = lonstr.Replace('.', ',');
                if (!Double.TryParse(latstr, out lat) || !Double.TryParse(lonstr, out lon))
                    throw new Exception(String.Format("invalid location: {0} {1}", latstr, lonstr));

                Location l = new Location(lat, lon);
                return l;
            }

            public LocationInfo(string t, string d, Location l)
            {
                type = t;
                desc = d;
                location = l;
            }

            public LocationInfo(string t, string d, double lat, double lon)
                : this(t, d, new Location(lat, lon))
            { }

            public LocationInfo(string t, string d, string latstr, string lonstr)
                : this(t, d, fromString(latstr, lonstr))
            { }
        }

        public MapWindow(params LocationInfo[] l)
        {
            InitializeComponent();
            locations = l;
            if (l.Length == 1)
                map.ZoomLevel += 1.0;
            Repaint();
        }

        private void Repaint()
        {
            double minlat = 0, maxlat = 0, minlon = 0, maxlon = 0;
            bool first = true;
            foreach (LocationInfo l in locations)
            {
                if (first == true)
                {
                    first = false;
                    minlat = maxlat = l.location.Latitude;
                    minlon = maxlon = l.location.Longitude;
                }
                if (minlat > l.location.Latitude)
                    minlat = l.location.Latitude;
                if (maxlat < l.location.Latitude)
                    maxlat = l.location.Latitude;
                if (minlon > l.location.Longitude)
                    minlon = l.location.Longitude;
                if (maxlon < l.location.Longitude)
                    maxlon = l.location.Longitude;

                Pushpin p = new Pushpin();
                p.Location = l.location;
                p.Content = l.type;
                p.ToolTip = l.desc;
                p.Foreground = new SolidColorBrush(Colors.Black);
                switch (l.type)
                {
                    case "T": p.Background = new SolidColorBrush(Colors.LightBlue); break;
                    case "F": p.Background = new SolidColorBrush(Colors.Pink); break;
                }
                map.Children.Add(p);
            }

            double avglat = (maxlat - minlat) / 2.0 + minlat;
            double avglon = (maxlon - minlon) / 2.0 + minlon;
            center = new Location(avglat, avglon);
            Center();
        }

        private void Center()
        {
            Console.WriteLine("centering the map to {0} {1}", center.Latitude, center.Longitude);
            map.Center = center;
        }

        private void ZoomPlusButton_Click(object sender, RoutedEventArgs e)
        {
            map.ZoomLevel++;
        }

        private void ZoomMinusButton_Click(object sender, RoutedEventArgs e)
        {
            map.ZoomLevel--;
        }

        private void CenterButton_Click(object sender, RoutedEventArgs e)
        {
            Center();
        }
    }
}
