﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DesktopClient
{
    /// <summary>
    /// Interaction logic for NotkaWindow.xaml
    /// </summary>
    public partial class NotkaWindow : Window
    {
        private Dictionary<String, ServiceReference1.FacultyCompositeType> map = new Dictionary<string, ServiceReference1.FacultyCompositeType>();
        private MainWindow parent;

        public NotkaWindow(MainWindow p)
        {
            InitializeComponent();
            var l = System.Windows.Application.GetCookie(new Uri("http://localhost:4705/Service1.svc"));
            ServiceReference1.FacultyCompositeType[] f = App.Server().GetFacultiesByTeacherUsername(l);
            foreach(var x in f)
            {
                map.Add(x.name, x);
                FacultyComboBox.Items.Add(x.name);
            }
            parent = p;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ServiceReference1.NoteCompositeType n = new ServiceReference1.NoteCompositeType();
            n.message = MessageTextBox.Text;
            var l = System.Windows.Application.GetCookie(new Uri("http://localhost:4705/Service1.svc"));
            var selected = FacultyComboBox.SelectedValue;
            if (selected == null)
                return;
            n.facultyId = this.map[selected.ToString()].id;
            n.teacherName = App.Server().GetTeacherByUsername(l).name;
            n.time = DateTime.Now.ToString();
            App.Server().AddNote(n);
            parent.refreshNotes();
            this.Content = null;
            this.Close();
        }
    }
}
