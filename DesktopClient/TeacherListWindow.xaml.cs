﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DesktopClient
{
    /// <summary>
    /// Interaction logic for TeacherListWindow.xaml
    /// </summary>
    public partial class TeacherListWindow : Window
    {
        public TeacherListWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            System.Windows.Data.CollectionViewSource teacherCompositeTypeViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("teacherCompositeTypeViewSource")));
            teacherCompositeTypeViewSource.Source = App.Server().GetTeachers();
            teacherCompositeTypeViewSource.DeferRefresh();
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            this.Content = null;
            Window fac = new MainWindow();
            this.Close();
            fac.Show();
        }

        void ShowDetails(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow)
                {
                    var row = (DataGridRow)vis;
                    DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(row);

                    if (presenter == null)
                    {
                        teacherCompositeTypeDataGrid.ScrollIntoView(row, teacherCompositeTypeDataGrid.Columns[0]);
                        presenter = GetVisualChild<DataGridCellsPresenter>(row);
                    }

                    DataGridCell TeacherNameCell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(0);
                    var teacherName = TeacherNameCell.Content as TextBlock;
                    Window fac = new TeacherWindow(teacherName.Text);
                    fac.Show();
                    break;
                }
        }

        public static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }
    }
}
