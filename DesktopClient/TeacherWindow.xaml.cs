﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DesktopClient
{
    /// <summary>
    /// Interaction logic for TeacherWindow.xaml
    /// </summary>
    public partial class TeacherWindow : Window
    {
        string TeacherName;
        private ServiceReference1.TeacherCompositeType teacher;

        public TeacherWindow(string teacher_name)
        {
            this.TeacherName = teacher_name;
            InitializeComponent();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            teacher = App.Server().GetTeacherByName(TeacherName);
            this.Name.Content = teacher.name;
            this.Email.Content = teacher.email;
            this.Description.Content = teacher.description;
            this.Location.Content = teacher.location.name;
            ServiceReference1.FacultyCompositeType[] f = App.Server().GetFacultiesByTeacher(teacher.id);
            foreach (var x in f)
            {
                this.Fakultety.Items.Add(x.name);
            }
        }

        private void Mapa_Click(object sender, RoutedEventArgs e)
        {
            Window map = new MapWindow(new MapWindow.LocationInfo("T", teacher.name, teacher.location.latitude, teacher.location.longitude));
            map.Show();
        }
    }
}
