using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Durum.DB.Models;
using MySql.Data.MySqlClient;

namespace Durum.DB.DAO
{
    public class FacultyDAO
    {
        public static void SignUp(int faculty_id, int user_id)
        {
            bool free_seats = false;
            int taken = 0;
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT COUNT(*) FROM StudentsFaculties WHERE StudentFacultyId='" + faculty_id.ToString() + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            if (dataReader.HasRows)
            {
                dataReader.Read();
                 taken= Int32.Parse(dataReader.GetValue(0).ToString());
            }
            dataReader.Close();
            command = new MySqlCommand("SELECT MaxStudents FROM Faculties WHERE Id='" + faculty_id.ToString() + "'", conn);
            dataReader = command.ExecuteReader();
            if (dataReader.Read())
            {
                var x = (int)dataReader[0];
                if (x > taken)
                    free_seats = true;
            }
            dataReader.Close();
            if (!free_seats)
            {
                var e = new TooManyStudentsException();
                throw new System.ServiceModel.FaultException<TooManyStudentsException>(e, e.ToString());
            }
            command = new MySqlCommand("SELECT * FROM StudentsFaculties WHERE StudentFacultyId='" + faculty_id.ToString() + "' AND UserId='" + user_id.ToString() + "'", conn);
            dataReader = command.ExecuteReader();
            if (dataReader.HasRows)
            {
                var e = new JuzZapisanyException();
                throw new System.ServiceModel.FaultException<JuzZapisanyException>(e, e.ToString());
            }
            else
            {
                dataReader.Close();
                command = new MySqlCommand("INSERT INTO StudentsFaculties(UserId, StudentFacultyId) VALUES ('" + user_id.ToString() + "','" + faculty_id.ToString() + "')", conn);
                command.ExecuteNonQuery();
            }
            
        }

        public static List<Faculty> getAllFaculties()
        {
            List<Faculty> facultyList = new List<Faculty>();
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Faculties", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            Faculty f = null;
            while (dataReader.Read())
            {
                LocationCompositeType l = LocationDAO.getLocationById((int)dataReader[3]);
                f = new Faculty((int)dataReader[0], (String)dataReader[1], (String)dataReader[2], l, (String)dataReader[4]);

                facultyList.Add(f);
            }
            return facultyList;
        }

        public static Faculty getFacultyById(int facultyId)
        {
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Faculties WHERE Id='" + facultyId.ToString() + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            Faculty f = null;
            if (dataReader.Read())
            {
                LocationCompositeType l = LocationDAO.getLocationById((int)dataReader[3]);
                f = new Faculty((int)dataReader[0], (String)dataReader[1], (String)dataReader[2], l, (String)dataReader[4]);
            }
            return f;
        }

        public static Faculty getFacultyByName(string facultyName)
        {
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Faculties WHERE NameStr='" + facultyName + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            Faculty f = null;
            if (dataReader.Read())
            {
                LocationCompositeType l = LocationDAO.getLocationById((int)dataReader[3]);
                f = new Faculty((int)dataReader[0], (String)dataReader[1], (String)dataReader[2], l, (String)dataReader[4]);
            }
            return f;
        }

        public static TeacherCompositeType[] getTeachersForFaculty(int facultyId)
        {
            List<TeacherCompositeType> teacherList = new List<TeacherCompositeType>();
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT TeacherId FROM TeachersFaculties WHERE TeacherFacultyId='" + facultyId.ToString() + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            TeacherCompositeType t = null;
            while (dataReader.Read())
            {
                t = TeacherDAO.getTeacherById((int)dataReader[0]);
                teacherList.Add(t);
            }
            return teacherList.ToArray();
        }

        public static List<Faculty> getFacultiesForTeacher(int teacherId)
        {
            List<Faculty> facultyList = new List<Faculty>();
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT TeacherFacultyId FROM TeachersFaculties WHERE TeacherId='" + teacherId.ToString() + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            Faculty f = null;
            while (dataReader.Read())
            {
                f = FacultyDAO.getFacultyById((int)dataReader[0]);
                facultyList.Add(f);
            }
            return facultyList;
        }

        public static List<Faculty> getFacultiesForTeacherUsername(string username)
        {
            User u = UserDAO.getUserByUsername(username);
            Teacher t = TeacherDAO.getTeacherByUserId(u.id);
            List<Faculty> facultyList = new List<Faculty>();
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT TeacherFacultyId FROM TeachersFaculties WHERE TeacherId='" + t.id.ToString() + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            Faculty f = null;
            while (dataReader.Read())
            {
                f = FacultyDAO.getFacultyById((int)dataReader[0]);
                facultyList.Add(f);
            }
            return facultyList;
        }

        internal static List<Faculty> getFacultiesForStudent(int studentId)
        {
            List<Faculty> facultyList = new List<Faculty>();
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT StudentFacultyId FROM StudentsFaculties WHERE StudentId='" + studentId.ToString() + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            Faculty f = null;
            while (dataReader.Read())
            {
                f = FacultyDAO.getFacultyById((int)dataReader[0]);
                facultyList.Add(f);
            }
            return facultyList;
        }
    }
}
