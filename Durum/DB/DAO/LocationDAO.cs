using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Durum.DB.Models;
using MySql.Data.MySqlClient;

namespace Durum.DB.DAO
{
    public class LocationDAO
    {
        public static List<LocationCompositeType> getAllLocations()
        {
            List<LocationCompositeType> locationList = new List<LocationCompositeType>();
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Locations", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            LocationCompositeType l = null;
            while (dataReader.Read())
            {
                l = new LocationCompositeType((int)dataReader[0], (String)dataReader[1], (String)dataReader[2], (String)dataReader[3]);
                locationList.Add(l);
            }
            return locationList;
        }

        public static LocationCompositeType getLocationById(int locationId)
        {
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Locations WHERE Id='" + locationId.ToString() + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            LocationCompositeType l = null;
            if (dataReader.Read())
            {
                l = new LocationCompositeType((int)dataReader[0], (String)dataReader[1], (String)dataReader[2], (String)dataReader[3]);
            }
            return l;
        }

        public static LocationCompositeType getLocationByName(String locationName)
        {
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Locations WHERE NameStr='" + locationName + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            LocationCompositeType l = null;
            if (dataReader.Read())
            {
                l = new LocationCompositeType((int)dataReader[0], (String)dataReader[1], (String)dataReader[2], (String)dataReader[3]);
            }
            return l;
        }
    }
}
