﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Durum.DB.Models;
using MySql.Data.MySqlClient;

namespace Durum.DB.DAO
{
    public class NoteDAO
    {
        public static NoteCompositeType[] getAllNotes()
        {
            List<NoteCompositeType> noteList = new List<NoteCompositeType>();
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Notes", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            NoteCompositeType f = null;
            while (dataReader.Read())
            {
                TeacherCompositeType l = TeacherDAO.getTeacherById((int)dataReader[1]);
                f = new NoteCompositeType((int)dataReader[0], (int)dataReader[2], l.name, (String)dataReader[3], (String)dataReader[4]);

                noteList.Add(f);
            }
            return noteList.ToArray();
        }

        public static void addNote(NoteCompositeType note)
        {
            Teacher t = TeacherDAO.getTeacherByName(note.teacherName);
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("INSERT INTO Notes(NoteTeacherId, NoteFacultyId, Message, WhenEst) VALUES ('" + t.id + "','" + note.facultyId + "','" + note.message + "','" + note.time + "')", conn);
            command.ExecuteNonQuery();
        }
    }
}