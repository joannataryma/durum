using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Durum.DB.Models;
using MySql.Data.MySqlClient;

namespace Durum.DB.DAO
{
    public class StudentDAO
    {
        public static List<Student> getAllStudents()
        {
            List<Student> studentList = new List<Student>();
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Students", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            Student s = null;
            while (dataReader.Read())
            {
                s = new Student((int)dataReader[0], (int)dataReader[1], (String)dataReader[2]);
                studentList.Add(s);
            }
            return studentList;
        }

        public static Student getStudentById(int id)
        {
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Students WHERE Id='" + id.ToString() + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            Student s = null;
            if (dataReader.Read())
            {
                s = new Student((int)dataReader[0], (int)dataReader[1], (String)dataReader[2]);
            }
            return s;
        }
    }
}
