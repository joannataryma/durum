using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Durum.DB.Models;
using MySql.Data.MySqlClient;

namespace Durum.DB.DAO
{
    public class TeacherDAO
    {
        public static List<Teacher> getAllTeachers()
        {
            List<Teacher> teacherList = new List<Teacher>();
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Teachers", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            Teacher t = null;
            while (dataReader.Read())
            {
                LocationCompositeType l = LocationDAO.getLocationById((int)dataReader[3]);
                t = new Teacher((int)dataReader[0], (int)dataReader[1], (String)dataReader[2], l, (String)dataReader[4], (String)dataReader[5]);
                teacherList.Add(t);
            }
            return teacherList;
        }

        public static Teacher getTeacherByName(string name)
        {
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Teachers WHERE NameStr='" + name + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            Teacher t = null;
            if (dataReader.Read())
            {
                LocationCompositeType l = LocationDAO.getLocationById((int)dataReader[3]);
                t = new Teacher((int)dataReader[0], (int)dataReader[1], (String)dataReader[2], l, (String)dataReader[4], (String)dataReader[5]);
            }
            return t;
        }
        public static TeacherCompositeType getTeacherById(int id)
        {
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Teachers WHERE Id='" + id.ToString() + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            TeacherCompositeType t = null;
            if (dataReader.Read())
            {
                LocationCompositeType l = LocationDAO.getLocationById((int)dataReader[3]);
                t = new TeacherCompositeType((int)dataReader[0], (int)dataReader[1], (string)dataReader[2], (string)dataReader[4], l, (string)dataReader[5]);
            }
            return t;
        }
        public static Teacher getTeacherByUserId(int id)
        {
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Teachers WHERE TeacherUserId='" + id.ToString() + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            Teacher t = null;
            if (dataReader.Read())
            {
                LocationCompositeType l = LocationDAO.getLocationById((int)dataReader[3]);
                t = new Teacher((int)dataReader[0], (int)dataReader[1], (String)dataReader[2], l, (String)dataReader[4], (String)dataReader[5]);
            }
            return t;
        }

    }
}
