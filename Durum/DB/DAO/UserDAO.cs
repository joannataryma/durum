using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Durum.DB.Models;
using MySql.Data.MySqlClient;

namespace Durum.DB.DAO
{
    public class UserDAO
    {
        public static List<User> getAllUsers()
        {
            List<User> userList = new List<User>();
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Users", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            User u = null;
            while (dataReader.Read())
            {
                u = new User((int)dataReader[0], (String)dataReader[1], (String)dataReader[2], (String)dataReader[3]);
                userList.Add(u);
            }
            return userList;
        }

        public static User getUserByUsername(String username)
        {
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("SELECT * FROM Users WHERE Username='" + username + "'", conn);
            MySqlDataReader dataReader = command.ExecuteReader();
            User u = null;
            if (dataReader.Read())
            {
                u = new User((int)dataReader[0], (String)dataReader[1], (String)dataReader[2], (String)dataReader[3]);
            }
            return u;
        }

        public static void updateUserPassword(UserCompositeType user)
        {
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("Update Users SET Password='" + user.password + "' WHERE Id='" + user.id + "'", conn);
            command.ExecuteNonQuery();
        }

        public static void addUser(UserCompositeType user)
        {
            MySqlConnection conn = null;
            conn = new MySqlConnection(Utils.CONNECTION_DATA);
            conn.Open();
            MySqlCommand command = new MySqlCommand("INSERT INTO Users(Username, Pass, Usertype) VALUES ('" + user.username + "','" + user.password + "','" + user.type + "')", conn);
            command.ExecuteNonQuery();
        }
    }
}
