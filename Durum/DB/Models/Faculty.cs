using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Durum.DB.Models
{
    public class Faculty
    {
        public Faculty(int id, String name, String time, LocationCompositeType l, String d)
        {
            this.id = id;
            this.time = time;
            this.name = name;
            this.location = l;
            this.description = d;
        }
        public int id { get; set; }
        public String time { get; set; }
        public String name { get; set; }
        public LocationCompositeType location { get; set; }
        public String description { get; set; }
    }
}
