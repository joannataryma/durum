using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Durum.DB.Models
{
    public class Location : LocationCompositeType
    {
        public Location(int id, String name, String latitude, String longitude) : base(id, name, latitude, longitude) { }
    }
}
