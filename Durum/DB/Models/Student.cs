using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Durum.DB.Models
{
    public class Student
    {
        public Student(int id, int userId, String name)
        {
            this.id = id;
            this.userId = userId;
            this.name = name;
        }
        int id { get; set; }
        int userId { get; set; }
        String name { get; set; }
    }
}
