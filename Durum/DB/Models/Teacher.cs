using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Durum.DB.Models
{
    public class Teacher
    {
        public Teacher(int id, int userId, String name, LocationCompositeType l, String d, String e)
        {
            this.id = id;
            this.userId = userId;
            this.name = name;
            this.location = l;
            this.description = d;
            this.email = e;
        }
        public int id { get; set; }
        public int userId { get; set; }
        public String name { get; set; }
        public LocationCompositeType location { get; set; }
        public String description;
        public String email;
    }
}
