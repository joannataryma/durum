using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Durum
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        string[] GetUsers();

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        [FaultContract(typeof(NotFoundException))]
        FacultyCompositeType[] GetFaculties();

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        [FaultContract(typeof(NotFoundException))]
        FacultyCompositeType GetFacultyById(int id);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        [FaultContract(typeof(NotFoundException))]
        FacultyCompositeType GetFacultyByName(string name);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        [FaultContract(typeof(NotFoundException))]
        LocationCompositeType[] GetLocations();

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        TeacherCompositeType[] GetTeachers();

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        TeacherCompositeType GetTeacherById(int id);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        TeacherCompositeType[] GetTeachersByFacultyName(string name);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        TeacherCompositeType GetTeacherByUsername(string username);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        TeacherCompositeType GetTeacherByName(string name);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        FacultyCompositeType[] GetFacultiesByTeacher(int teacher_id);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        FacultyCompositeType[] GetFacultiesByTeacherUsername(string teacher_username);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        [FaultContract(typeof(NotFoundException))]
        bool CheckUser(string username, string pass);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        string CheckUserPrivileges(string username);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        void RegisterUser(UserCompositeType u);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        [FaultContract(typeof(NotFoundException))]
        LocationCompositeType GetLocationById(int id);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        [FaultContract(typeof(NotFoundException))]
        LocationCompositeType GetLocationByName(string name);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        void AddNote(NoteCompositeType n);

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        [FaultContract(typeof(NotFoundException))]
        NoteCompositeType[] GetAllNotes();

        [OperationContract]
        [FaultContract(typeof(DAOFaultContext))]
        [FaultContract(typeof(NotFoundException))]
        bool SignUp(int faculty_id, string username);
    }

    [DataContract]
    public class TeacherCompositeType
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int userId { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public LocationCompositeType location { get; set; }
        [DataMember]
        public string email { get; set; }
        public TeacherCompositeType() { }
        public TeacherCompositeType(int id, int uid, string name, string desc, LocationCompositeType loc, string email)
        {
            this.id = id;
            this.userId = uid;
            this.name = name;
            this.description = desc;
            this.location = loc;
            this.email = email;
        }
    }

    [DataContract]
    public class NoteCompositeType
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int facultyId { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public string teacherName { get; set; }
        [DataMember]
        public string time { get; set; }
        public NoteCompositeType() { }
        public NoteCompositeType(int id, int fid, string teacherName, string message, string time)
        {
            this.id = id;
            this.facultyId = fid;
            this.teacherName = teacherName;
            this.message = message;
            this.time = time;
        }
    }

    [DataContract]
    public class UserCompositeType
    {
        public UserCompositeType(int id, string username, string password, string type)
        {
            this.id = id;
            this.username = username;
            this.password = password;
            this.type = type;
        }
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string type { get; set; }
    }
    
    [DataContract]
    public class FacultyCompositeType
    {
        public FacultyCompositeType(int id, string time, string name, LocationCompositeType l, string description)
        {
            this.id = id;
            this.time = time;
            this.name = name;
            this.location = l;
            this.description = description;
        }

        [DataMember]
        public int id { get; set; }
        [DataMember]
        public String time { get; set; }
        [DataMember]
        public String name { get; set; }
        [DataMember]
        public LocationCompositeType location { get; set; }
        [DataMember]
        public String description { get; set; }
    }

    [DataContract]
    public class LocationCompositeType
    {
        public LocationCompositeType(int id, String name, String latitude, String longitude)
        {
            this.id = id;
            this.name = name;
            this.latitude = latitude;
            this.longitude = longitude;
        }
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public String name { get; set; }
        [DataMember]
        public String latitude { get; set; }
        [DataMember]
        public String longitude { get; set; }
    }

    [DataContract]
    public class DAOFaultContext
    {
        public DAOFaultContext(String dao, String reason)
        {
            this.message = "Sorry, something horribly wrong happened with our database : (";
            this.dao = dao;
            this.reason = reason;
        }
        public override string ToString()
        {
            return message;
        }
        [DataMember]
        public String message;
        [DataMember]
        public String dao;
        [DataMember]
        public String reason;
    }

    [DataContract]
    public class NotFoundException
    {
        public NotFoundException(String what)
        {
            this.what = what;
        }
        public NotFoundException(String what, String info) : this(what)
        {
            this.info = info;
        }
        public override string ToString()
        {
            String s = "Requested record (" + what + ") not found";
            if (info != null)
            {
                s += Environment.NewLine + "More info: " + info;
            }
            return s;
        }
        [DataMember]
        public String what;
        [DataMember]
        public String info;
    }

    [DataContract]
    public class TooManyStudentsException
    {
        public TooManyStudentsException()
        {
        }
        public override string ToString()
        {
            String s = "There are no free seats on selected course.";
            return s;
        }
        [DataMember]
        public String what;
    }
    [DataContract]
    public class JuzZapisanyException
    {
        public JuzZapisanyException()
        {
        }
        public override string ToString()
        {
            String s = "You alredy signed up for this course.";
            return s;
        }
        [DataMember]
        public String what;
    }
}
