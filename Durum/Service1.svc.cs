using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Durum.DB.DAO;
using Durum.DB.Models;

namespace Durum
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string[] GetUsers()
        {
            try
            {
                List<User> users = UserDAO.getAllUsers();
                String[] ret = new String[users.Count];
                int i = 0;
                foreach (User u in users)
                {
                    ret[i] = u.username;
                    i++;
                }
                return ret;
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("UserDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }
        }

        public LocationCompositeType[] GetLocations()
        {
            List<LocationCompositeType> locations;
            try
            {
                locations = LocationDAO.getAllLocations();
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("LocationDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }

            if (locations.Count == 0)
            {
                NotFoundException e = new NotFoundException("any location", "Apocalypse?");
                throw new FaultException<NotFoundException>(e, e.ToString());
            }

            LocationCompositeType[] ret = new LocationCompositeType[locations.Count];
            int i = 0;
            foreach (LocationCompositeType l in locations)
            {
                ret[i] = l;
                i++;
            }
            return ret;
        }

        public FacultyCompositeType GetFacultyById(int id)
        {
            Faculty f;
            FacultyCompositeType l;
            try
            {
                f = FacultyDAO.getFacultyById(id);
                l = new FacultyCompositeType(f.id, f.time, f.name, f.location, f.description);
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("FacultyDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }

            if (l == null)
            {
                NotFoundException e = new NotFoundException("faculty: id " + id);
                throw new FaultException<NotFoundException>(e, e.ToString());
            }

            return l;
        }

        public FacultyCompositeType GetFacultyByName(string name)
        {
            Faculty f;
            FacultyCompositeType l;
            try
            {
                f = FacultyDAO.getFacultyByName(name);
                l = new FacultyCompositeType(f.id, f.time, f.name, f.location, f.description);
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("FacultyDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }

            if (l == null)
            {
                NotFoundException e = new NotFoundException("faculty: name " + name);
                throw new FaultException<NotFoundException>(e, e.ToString());
            }

            return l;
        }
        public TeacherCompositeType GetTeacherById(int id)
        {
            TeacherCompositeType t;
            try
            {
                t = TeacherDAO.getTeacherById(id);
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("TeacherDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }

            if (t == null)
            {
                NotFoundException e = new NotFoundException("teracher: id " + id);
                throw new FaultException<NotFoundException>(e, e.ToString());
            }

            return t;
        }

        public TeacherCompositeType[] GetTeachersByFacultyName(string name)
        {
            TeacherCompositeType[] t;
            try
            {
                var f = FacultyDAO.getFacultyByName(name);
                t = FacultyDAO.getTeachersForFaculty(f.id);
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("TeacherDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }

            if (t == null)
            {
                NotFoundException e = new NotFoundException("Faculty: name " + name);
                throw new FaultException<NotFoundException>(e, e.ToString());
            }

            return t;
        }

        public FacultyCompositeType[] GetFaculties()
        {
            try
            {
                List<Faculty> faculties = FacultyDAO.getAllFaculties();
                FacultyCompositeType[] ret = new FacultyCompositeType[faculties.Count];
                int i = 0;
                foreach (Faculty f in faculties)
                {
                    FacultyCompositeType tct = new FacultyCompositeType(
                        f.id, f.time, f.name, f.location, f.description);
                    ret[i++] = tct;
                }
                return ret;
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("FacultyDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }
        }

        public FacultyCompositeType[] GetFacultiesByTeacher(int  teacher_id)
        {
            try
            {
                List<Faculty> faculties = FacultyDAO.getFacultiesForTeacher(teacher_id);
                FacultyCompositeType[] ret = new FacultyCompositeType[faculties.Count];
                int i = 0;
                foreach (Faculty f in faculties)
                {
                    FacultyCompositeType tct = new FacultyCompositeType(
                        f.id, f.time, f.name, f.location, f.description);
                    ret[i++] = tct;
                }
                return ret;
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("FacultyDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }
        }

        public FacultyCompositeType[] GetFacultiesByTeacherUsername(string teacher_username)
        {
            try
            {
                List<Faculty> faculties = FacultyDAO.getFacultiesForTeacherUsername(teacher_username);
                FacultyCompositeType[] ret = new FacultyCompositeType[faculties.Count];
                int i = 0;
                foreach (Faculty f in faculties)
                {
                    FacultyCompositeType tct = new FacultyCompositeType(
                        f.id, f.time, f.name, f.location, f.description);
                    ret[i++] = tct;
                }
                return ret;
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("FacultyDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }
        }
        public TeacherCompositeType[] GetTeachers()
        {
            try
            {
                List<Teacher> teachers = TeacherDAO.getAllTeachers();
                TeacherCompositeType[] ret = new TeacherCompositeType[teachers.Count];
                int i = 0;
                foreach(Teacher t in teachers)
                {
                    TeacherCompositeType tct = new TeacherCompositeType(
                        t.id, t.userId, t.name, t.description, t.location, t.email);
                    ret[i++] = tct;
                }
                return ret;
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("TeacherDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }
        }


        public bool CheckUser(string username, string pass)
        {
            User u;
            try
            {
                u = UserDAO.getUserByUsername(username);
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("UserDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }

            if (u == null)
            {
                NotFoundException e = new NotFoundException("user: " + username);
                throw new FaultException<NotFoundException>(e, e.ToString());
            }

            return u.password == pass;
        }

        public string CheckUserPrivileges(string username)
        {
            try
            {
                User u = UserDAO.getUserByUsername(username);
                return u.type;
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("UserDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }
        }

        public void RegisterUser(UserCompositeType u)
        {
            try
            {
                UserDAO.addUser(u);
                return;
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("UserDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }
        }

        public LocationCompositeType GetLocationById(int id)
        {
            LocationCompositeType l;
            try
            {
                l = LocationDAO.getLocationById(id);
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("LocationDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }

            if (l == null)
            {
                NotFoundException e = new NotFoundException("location: id " + id);
                throw new FaultException<NotFoundException>(e, e.ToString());
            }

            return l;
        }

        public LocationCompositeType GetLocationByName(string name)
        {
            LocationCompositeType l;
            try
            {
                l = LocationDAO.getLocationByName(name);
            }
            catch (Exception e)
            {
                DAOFaultContext c = new DAOFaultContext("LocationDAO", e.Message);
                throw new FaultException<DAOFaultContext>(c, c.ToString());
            }

            if (l == null)
            {
                NotFoundException e = new NotFoundException("location: " + name);
                throw new FaultException<NotFoundException>(e, e.ToString());
            }

            return l;
        }

        public void AddNote(NoteCompositeType n)
        {
            NoteDAO.addNote(n);
        }

        public TeacherCompositeType GetTeacherByUsername(string username)
        {
            UserCompositeType u = UserDAO.getUserByUsername(username);
            Teacher t = TeacherDAO.getTeacherByUserId(u.id);
            TeacherCompositeType tct = new TeacherCompositeType(t.id, t.userId, t.name, t.description, t.location, t.email);
            return tct;
        }

        public TeacherCompositeType GetTeacherByName(string name)
        {
            Teacher t = TeacherDAO.getTeacherByName(name);
            TeacherCompositeType tct = new TeacherCompositeType(t.id, t.userId, t.name, t.description, t.location, t.email);
            return tct;
        }

        public NoteCompositeType[] GetAllNotes()
        {
            return NoteDAO.getAllNotes();
        }

        public bool SignUp(int faculty_id, string username)
        {
            int user_id = UserDAO.getUserByUsername(username).id;
            FacultyDAO.SignUp(faculty_id, user_id);
            return true;
        }
    }
}
