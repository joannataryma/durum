﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace PhoneApp1
{
    public partial class MainPage : PhoneApplicationPage
    {

        private bool ok = false;

        private void client_checkUserCompleted
        (object sender, ServiceReference1.CheckUserCompletedEventArgs e)
        {

        }

        public MainPage()
        {
            InitializeComponent();
            App.Server().CheckUserCompleted += new EventHandler
            <ServiceReference1.CheckUserCompletedEventArgs>(client_checkUserCompleted);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

            var login = textBox1.Text;
            var password = passwordBox1.Password;
            if (login != "")
                App.Server().CheckUserAsync(login, password);
         
        }

    }
}