DROP TABLE IF EXISTS mysql56.Notes;
DROP TABLE IF EXISTS mysql56.StudentsFaculties;
DROP TABLE IF EXISTS mysql56.TeachersFaculties;
DROP TABLE IF EXISTS mysql56.Students;
DROP TABLE IF EXISTS mysql56.Faculties;
DROP TABLE IF EXISTS mysql56.Teachers;
DROP TABLE IF EXISTS mysql56.Locations;
DROP TABLE IF EXISTS mysql56.Users;

CREATE TABLE IF NOT EXISTS mysql56.Locations(Id INT PRIMARY KEY AUTO_INCREMENT,
    NameStr VARCHAR(50) UNIQUE,
	Latitude VARCHAR(50),
	Longtitude VARCHAR(50))
	ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS mysql56.Users(Id INT PRIMARY KEY AUTO_INCREMENT,
	Username VARCHAR(25) UNIQUE,
	Pass VARCHAR(100),
	Usertype VARCHAR(10))
	ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS mysql56.Students(Id INT PRIMARY KEY AUTO_INCREMENT,
	StudentUserId int,
	NameStr VARCHAR(100),
	CONSTRAINT fk_StudentUserId FOREIGN KEY (StudentUserId) REFERENCES Users(Id))
	ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS mysql56.Teachers(Id INT PRIMARY KEY AUTO_INCREMENT,
	TeacherUserId int,
	NameStr VARCHAR(100) UNIQUE,
	TeacherLocationId int,
    Description VARCHAR(255),
    Email VARCHAR(100),
		CONSTRAINT fk_TeacherUserId FOREIGN KEY (TeacherUserId) REFERENCES Users(Id),
		CONSTRAINT fk_TeacherLocationId FOREIGN KEY (TeacherLocationId) REFERENCES Locations(Id))
	ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS mysql56.Faculties(Id INT PRIMARY KEY AUTO_INCREMENT,
	NameStr VARCHAR(100) UNIQUE,
    WhenStr VARCHAR(100),
    FacultyLocationId int,
    Description VARCHAR(255),
	MaxStudents INT DEFAULT 2,
	CONSTRAINT fk_FacultyLocationId FOREIGN KEY (FacultyLocationId) REFERENCES Locations(Id))
	ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS mysql56.TeachersFaculties(
	TeacherId int,
	TeacherFacultyId int,
	CONSTRAINT fk_TeacherId FOREIGN KEY (TeacherId) REFERENCES Teachers(Id),
	CONSTRAINT fk_TeacherFacultyId FOREIGN KEY (TeacherFacultyId) REFERENCES Faculties(Id))
	ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS mysql56.StudentsFaculties(
	UserId int,
	StudentFacultyId int,
	CONSTRAINT fk_UserId FOREIGN KEY (UserId) REFERENCES Users(Id),
	CONSTRAINT fk_StudentFacultyId FOREIGN KEY (StudentFacultyId) REFERENCES Faculties(Id))
	ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS mysql56.Notes(Id INT PRIMARY KEY AUTO_INCREMENT,
	NoteTeacherId int,
	NoteFacultyId int,
    Message VARCHAR(2000),
    WhenEst VARCHAR(100), 
	CONSTRAINT fk_NoteTeacherId FOREIGN KEY (NoteTeacherId) REFERENCES Teachers(Id),
	CONSTRAINT fk_NoteFacultyId FOREIGN KEY (NoteFacultyId) REFERENCES Faculties(Id))
	ENGINE=INNODB;

INSERT INTO mysql56.Users(Id, Username, Pass, Usertype) VALUES(1, 'asia', 'asiaPass', 'teacher');
INSERT INTO mysql56.Users(Id, Username, Pass, Usertype) VALUES(2, 'basia', 'basiaPass', 'teacher');
INSERT INTO mysql56.Users(Id, Username, Pass, Usertype) VALUES(3, 'maciek', 'maciekPass', 'student');
INSERT INTO mysql56.Users(Id, Username, Pass, Usertype) VALUES(4, 'marek', 'marekPass', 'student');
INSERT INTO mysql56.Users(Id, Username, Pass, Usertype) VALUES(5, 'jerzy', 'jerzyPass', 'student');

INSERT INTO mysql56.Locations(Id, NameStr, Latitude, Longtitude) VALUES(1, 'cosNaNE', '54.371421', '18.612529');
INSERT INTO mysql56.Locations(Id, NameStr, Latitude, Longtitude) VALUES(2, 'cosNaEA', '54.37099', '18.612674');
INSERT INTO mysql56.Locations(Id, NameStr, Latitude, Longtitude) VALUES(3, 'cosNaZiE', '54.370274', '18.616702');
INSERT INTO mysql56.Locations(Id, NameStr, Latitude, Longtitude) VALUES(4, 'cosNaNovum', '54.372787', '18.616536');

INSERT INTO mysql56.Teachers(Id, TeacherUserId, NameStr, TeacherLocationId, Description, Email) VALUES (1, 1, "Joanna Klops", 3, "Gry i zabawy!", "joanna.klops@pegie.gda.pl");

INSERT INTO mysql56.Faculties(Id, NameStr, WhenStr, FacultyLocationId, Description) VALUES (1, "Silikonowa dolina", "Wtorek 18.10", 1, "Krzemek");

INSERT INTO mysql56.TeachersFaculties(TeacherId, TeacherFacultyId) VALUES (1, 1);

INSERT INTO mysql56.StudentsFaculties(UserId, StudentFacultyId) VALUES (3, 1);
select count(*) from mysql56.teachersfaculties where teacherfacultyid=1;
SELECT COUNT(*) FROM mysql56.StudentsFaculties WHERE StudentFacultyId='1';
SELECT MaxStudents FROM mysql56.Faculties WHERE Id='1'